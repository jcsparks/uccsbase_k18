

jQuery(document).ready(function () {
    buildDeptSelect();
    
    handleDeptSelect();
});


function buildDeptSelect() {
    // find all of the .uccs-dept-page-title elements
    var depts    = {};
    var deptId   = '';
    var deptName = '';
    jQuery('.uccs-dept-page-title').each(function () {
        // grab their IDs
        deptId = jQuery(this).attr('id');
        deptName = jQuery(this).find('h2').html();

        depts[deptId] = deptName;
    });

    // see if we have any depts to work with
    if(Object.keys(depts).length > 1) {
        // add a default value
        jQuery('#uccs-department-selector').append(jQuery("<option></option>").attr("value", '').text('Jump to ...'));

        // add options to the select list based on these IDs.
        jQuery.each(depts, function (id, name) {
            jQuery('#uccs-department-selector').append(jQuery("<option></option>").attr("value", '#' + id).text(name));
        });

    } else {
        // hide the select
        jQuery('.uccs-dept-selector-container ').hide();
    }
}

function handleDeptSelect() {
    jQuery('#uccs-department-selector').change(function () {
        if(jQuery(this).val()) {
            jQuery(window).scrollTo(jQuery(this).val(), 500);
        }
    })
}