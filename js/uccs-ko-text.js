
function renderKnockoutText(slideText, slideColor) {
    //console.log("Attempting to render text " + slideText + " in color " + slideColor);
    if(!slideText) {
        return;
    }
    var canvas = document.getElementById('ko-text-mask');
    if(!canvas){
        return;
    }

    // slideText = "There can be only one!";

    //jQuery('#ko-text-mask').detach().appendTo('#heroSlider');

    var parentWidth = document.getElementById('UCCSspKOContainer_').offsetWidth;
    var parentHeight = document.getElementById('UCCSspKOContainer_').offsetHeight;

    // fractional pixels seem to cause issues with canvas rendering over an image (you'll see a bit of pixel showing
    //  at the top or bottom edge)  So we're making the canvas one pixel larger to cover any potential fractional
    //  pixels.
    parentHeight = parentHeight + 1;

    jQuery('#ko-text-mask').css('height', parentHeight + 'px');

    var ctx = canvas.getContext("2d");
    if(!ctx) {
        return;
    }
    if(!slideColor) {
        slideColor = "rgba(255,255,255,1)";
    }

    ctx.canvas.width = parentWidth;
    ctx.canvas.height = parentHeight;

    var fontScale = parseInt(canvas.height / 2);
    ctx.font = "900 " + fontScale + "px 'Helvetica'";
    var shrinkage = -1;

    // figure out if the rendered text will fit inside the parent element and make it 10% smaller iteratively
    // until it does fit.
    do {
        //console.log("Trying FontScale : " + fontScale);
        shrinkage++;
        fontScale = parseInt(fontScale * .9);
        ctx.font = "900 " + fontScale + "px 'Helvetica'";
    } while ((ctx.measureText(slideText.toUpperCase()).width > parseInt(parentWidth * 0.9) ) && ctx.measureText(slideText.toUpperCase()).width > 580);

    // if we didn't have to shrink the text at all... it's also possible that we could go bigger
    if(!shrinkage) {
        do {
            //console.log("Trying FontScale : " + fontScale);
            fontScale = parseInt(fontScale * 1.1);
            ctx.font = "900 " + fontScale + "px 'Helvetica'";
        } while ((ctx.measureText(slideText.toUpperCase()).width < parseInt(parentWidth * 0.9) ) && (fontScale < parseInt(canvas.height * 0.75)));
    }

    //console.log("FontScale is set at : " + fontScale);


    if(parentWidth >= 577) {

        // white fill of canvas
        ctx.fillStyle = slideColor;
        ctx.clearRect(0,0,canvas.width,canvas.height);
        ctx.fillRect(0,0,canvas.width,canvas.height);

        // gold box render
        ctx.fillStyle = "rgba(207, 184, 123, 0.7)";
        ctx.fillRect(parentWidth * 0.1,0, parentWidth * 0.4, canvas.height);

        // render the text on a single line
        ctx.fillStyle = slideColor;
        ctx.globalCompositeOperation = 'destination-out';
        ctx.textAlign = 'center';
        ctx.fillText(slideText.toUpperCase(), canvas.width / 2, (canvas.height / 2 + fontScale / 2 ) );
    } else {

        var textLines = slideText.split(' ');
        var newCanvasHeight = textLines.length * 85;
        if(textLines.length < 2) {
            newCanvasHeight = 125;
        }

        jQuery('.uccs-sp-ko-img-container').css('height', newCanvasHeight + 'px');
        jQuery('canvas#ko-text-mask').css('height', newCanvasHeight + 'px');

        ctx.canvas.height = newCanvasHeight;

        // white fill of canvas
        ctx.fillStyle = slideColor;
        ctx.clearRect(0,0,canvas.width,canvas.height);
        ctx.fillRect(0,0,canvas.width,canvas.height);

        // render the text on multiple lines
        ctx.globalCompositeOperation = 'destination-out';
        ctx.font = "900 92px 'Helvetica'";
        wrapText(ctx, slideText.toUpperCase(), 20, 100, parentWidth, 72);
    }

}

function KOText() {
    var koTextTitle = jQuery('#uccs-ko-text-title').data('title');
    //renderKnockoutText(jQuery('.rsActiveSlide').children().first().data("title"), jQuery('.rsActiveSlide').children().first().data("color"));
    if(koTextTitle) {
        renderKnockoutText(koTextTitle, 'white');
    }
}


jQuery(window).on('load', function () {
    KOText();

    // var resizeID;
    // jQuery(window).resize(function () {
    //     clearTimeout(resizeID);
    //
    //     resizeID = setTimeout(KOText, 100);
    // });

     window.addEventListener('resize', KOText, false);

    //crazy mutation observer code :  SEE https://stackoverflow.com/questions/19401633/how-to-fire-an-event-on-class-change-using-jquery
    // var $slider = jQuery(".rsSlide");
    // var $numSlides = $slider.length;
    // var observer = new MutationObserver(function (mutations) {
    //     mutations.forEach(function (mutation) {
    //         if(mutation.attributeName === "class") {
    //             //var attributeValue = jQuery(mutation.target).prop(mutation.attributeName);
    //             //console.log("Class attribute changed to:", attributeValue);
    //             KOText();
    //         }
    //     });
    // });

    // for(var i = 0; i < $numSlides; i++) {
    //     observer.observe($slider[i], {
    //         attributes: true
    //     });
    // }

});

function wrapText(context, text, x, y, maxWidth, lineHeight) {
    var words = text.split(' ');
    var line = '';

//    alert("You gave me : " + text + " to work with");

    // for(var n = 0; n < words.length; n++) {
    //     var testLine = line + words[n] + ' ';
    //     var metrics = context.measureText(testLine);
    //     var testWidth = metrics.width;
    //     if (testWidth > maxWidth && n > 0) {
    //         context.fillText(line, x, y);
    //         line = words[n] + ' ';
    //         y += lineHeight;
    //     }
    //     else {
    //         line = testLine;
    //     }
    // }
    // context.fillText(line, x, y);


    for(var j = 0; j < words.length; j++) {

        line = words[j];

        context.fillText(line, x, y);

        y += lineHeight;
    }

}
