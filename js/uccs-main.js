

jQuery(document).ready(function () {

    // handler to deal with displaying the search form input when they click on the search icon/button
    // and submitting the form when they've typed into the input box and clicked
    SearchBoxHandler();

    // handler to display the jumbo navigation when you click on the main menu links for those menus
    JumboNavToggle();

    // handler to display and hide the utility menu when clicked on
    UtilMenuToggle();

    // handler to open/close sub-menus on a sidebar
    sidebarMenuToggle();

    // handler to deal with opening/closing Flyout sidebars
    sidebarFlyoutToggle();

    // handle the dropdown menu for the Utility Menu
    jQuery('li.util-menu-item.dropdown a.dropdown-toggle').click(function(e){
        e.preventDefault();
        jQuery(this).find('ul.dropdown-menu').show();
    });

    // handler to set the background image in a CTA Media button in the Jumbo Navigation CTA Region
    getCTA_bg_img();

    //handler to do stuff to the footer content block
    // footerContentBlock();
    // var resizeTimer;
    // jQuery(window).on('resize', function(e) {
    //
    //     clearTimeout(resizeTimer);
    //     resizeTimer = setTimeout(function() {
    //
    //         // Run code here, resizing has "stopped"
    //         footerContentBlock();
    //
    //     }, 250);
    // });

    // handler to deal with the toggling the menus on mobile view
    jQuery('a#UCCSmobileMenuToggle').click(function (e) {
        e.preventDefault();

        jQuery('.uccs-header-lower-container').slideToggle(1000);
    });

    // handler to close the local tasks (edit, revisions, clone, etc...)
    jQuery('a#uccsCloseLocalTasks').click(function (e) {
        e.preventDefault();

        jQuery('.uccs-local-tasks').hide();
    });

    // script to handle the scrollTop functionality
    // jQuery('.scrollTop').click(function(e){
    //     e.preventDefault();
    //
    //     jQuery("html,body").animate({scrollTop: 0}, "slow");
    // });

    // jQuery(document).scroll(function(){
    //     if(jQuery(this).scrollTop() > 100 ){
    //         jQuery('div.scrollTop').fadeIn();
    //     }else {
    //         jQuery('div.scrollTop').fadeOut();
    //     }
    //
    //     // check to see if we're scrolling down into the footer area
    //     if((jQuery(this).scrollTop() + jQuery('footer').height()) > (jQuery(document).height() - jQuery(window).height())) {
    //         var myFloor = jQuery('footer').height() - (jQuery(document).height() - jQuery(window).height() - jQuery(this).scrollTop());
    //         jQuery('div.scrollTop').css('bottom', myFloor);
    //     }else {
    //         jQuery('div.scrollTop').css('bottom', '3em');
    //     }
    // });
});

/*  SiteImprove Code */
(function() {
    var sz = document.createElement('script'); sz.type = 'text/javascript'; sz.async = true;
    sz.src = '//siteimproveanalytics.com/js/siteanalyze_51848.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(sz, s);
})();


/***************************************************************************************
 *  SearchBoxHandler() -
 *
 *  Handle displaying and hiding the Search Box element
 ***************************************************************************************/
function SearchBoxHandler() {
    jQuery('.uccs-search-btn').click(function(){

        document.forms['cse-search-box-form-id'].submit();
        return;

        // if(jQuery('button.uccs-search-btn').hasClass('active')) {
        //     //check to see if they've typed anything in the search box
        //     if(document.forms['cse-search-box-form-id'].search.value === "") {
        //         jQuery('button.uccs-search-btn').removeClass('active');
        //
        //         jQuery('input#UCCSsearchBox').animate({
        //             opacity: 0
        //         }, 1000);
        //
        //     } else {
        //         document.forms['cse-search-box-form-id'].submit();
        //     }
        // } else {
        //     jQuery('button.uccs-search-btn').addClass('active');
        //
        //     jQuery('input#UCCSsearchBox').animate({
        //         opacity: 1.0
        //     }, 1000);
        //
        //     jQuery('#UCCSsearchBox').focus();
        // }
    });
}

/***************************************************************************************
 *  UtilMenuToggle() -
 *
 *  Handle displaying and hiding the Utility Menu
 ***************************************************************************************/
function UtilMenuToggle() {

    jQuery('.util-menu-toggle-button').click(function (e) {
        jQuery('div#uccs-header-utility-menu').slideToggle(200);

        if(jQuery('.util-menu-toggle-button').hasClass('active')) {
            jQuery('.util-menu-toggle-button').removeClass('active');
        }else {
            jQuery('.util-menu-toggle-button').addClass('active');
        }

    });
}


/***************************************************************************************
 *  JumboNavToggle() -
 *
 *  Handle displaying and hiding the Jumbo Navigation area
 ***************************************************************************************/
function JumboNavToggle() {
    jQuery('.jumbo-link-toggle').click(function (e) {
        e.preventDefault();
        var target_id = jQuery(this).attr('href');

        var anchor_index  = target_id.indexOf('#');

        if(target_id && anchor_index) {
            target_id = target_id.substring(anchor_index);
        }

        var parent_id = '#' + jQuery(target_id).closest('.jumbo-nav-title').attr('id');
        //var cta_selector = "'" + parent_id + " .uccs-jumbo-nav-cta-block'";
        //var cta_selector = '#jumbo-nav-' + jQuery(this).parent().index();

        // This will return the width of the viewport
        var frameWidth = window.innerWidth;

        // get the id of the CTA Block (unique but machine generated)
        var cta_block = '#' + jQuery(parent_id).find('.uccs-jumbo-nav-cta-block').attr('id');


        // close the menu if they're clicking on the currently active one
        if(jQuery(this).hasClass('active')) {
            jQuery(target_id).slideToggle(500);
            jQuery(cta_block).slideToggle(500);
            jQuery('.jumbo-link-toggle').removeClass('active');
            jQuery('.jumbo-link-toggle svg.fa-minus').addClass('fa-plus').removeClass('fa-minus');
            return;
        }

        // reset the Jumbo Nav setup
        jQuery('.jumbo-navigation-container').removeClass('active');
        jQuery('.jumbo-navigation-container').hide();
        jQuery('.uccs-jumbo-nav-cta-block').hide();
        jQuery('.jumbo-link-toggle').removeClass('active');
        jQuery('.jumbo-link-toggle svg.fa-minus').addClass('fa-plus').removeClass('fa-minus');


        // set the newly selected menu as active
        jQuery(this).addClass('active');
        jQuery(target_id).addClass('active');

        //toggle the plus icon to a minus on the menu toggle
        jQuery('.jumbo-link-toggle.active svg').removeClass('fa-plus');
        jQuery('.jumbo-link-toggle.active svg').addClass('fa-minus');

        //figure out how tall the Jumbo Nav is
        // var jumbo_height = jQuery(target_id).height() + 10;
        // jumbo_height = '-' + jumbo_height + 'px';

        if(frameWidth < 577) {
            // move the DOM element for the Jumbo Nav inside the <ul> for mobile view
            var JumboNav = jQuery(parent_id).detach();
            jQuery(JumboNav).insertAfter(this);
        }else {
            // move the DOM element for the Jumbo Nav inside the <ul> for mobile view
            var JumboNav = jQuery(parent_id).detach();
            jQuery(JumboNav).insertAfter('header');

            if(frameWidth > 1199) {
                //set the margin top to the (negative) height of the Jumbo Nav so it lines up correctly
                // but only on the desktop view
                //jQuery(cta_block).css('margin-top', jumbo_height);
            }
        }


        // everything is setup, so now show it to the user
        jQuery(target_id).slideToggle(500);
        if(cta_block != '#undefined') {
            jQuery(cta_block).slideToggle(500);
        }

        // jQuery('#block-uccsbase-k18-content').animate({
        //     marginTop: jumbo_height
        // }, 1000);

        // check to see if the utility menu is also out and close it if so
        if(jQuery('.util-menu-toggle-button').hasClass('active')) {
//            jQuery('div#uccs-header-utility-menu').slideToggle(1000);
            jQuery('div#uccs-header-utility-menu').hide();
            jQuery('.util-menu-toggle-button').removeClass('active');
        }
    });
}


/***************************************************************************************
 *  getCTA_bg_img() -
 *
 *  Get the background image for CTA Button in the Jumbo Nav
 ***************************************************************************************/
function getCTA_bg_img() {

    jQuery('.uccs-jumbo-nav-cta-button').each(function (index, element) {
        //find the img src inside this cta button
        var img_url = jQuery(element).find(('img')).prop('src');

        // if no image, check for a background color
        if(!img_url) {
            var bg_color = jQuery(element).data('bgcolor') || '#DEDEDE';

            jQuery(element).css('background-color', bg_color);
        } else {
            jQuery(element).css('background-image', 'url(' + img_url + ')');
        }

        // check for an icon color
        var icon_color = jQuery(element).data('iconcolor') || '#CFB87B';
        jQuery(element).find('.uccs-main-menu-cta-icon').css('color', icon_color);

    });
}


/***************************************************************************************
 *  footerContentBlock() -
 *
 *  Dynamically style the left border on the #UCCSfooterContent element
 ***************************************************************************************/
function footerContentBlock() {

    var element = document.getElementById('UCCSfooterContent');

    if(element) {
        var domRect = element.getBoundingClientRect();
        jQuery(element).css('margin-left', '-' + domRect.left + 'px');
        jQuery(element).css('border-left', domRect.left + 'px solid #333333');
    }
}

/***************************************************************************************
 *  sidebarMenuToggle() -
 *
 *  Open and close the any sub-menus on a sidebar menu
 ***************************************************************************************/
function sidebarMenuToggle() {
    jQuery('.sidebar-menu-dropdown .uccs-dropdown-toggle').click(function (e) {
        e.preventDefault();

        // find the nearest dropdown menu element
        jQuery(this).parent().children('.uccs-sidebar-dropdown-menu').toggleClass('active');

        if(jQuery(this).find('svg.fa-chevron-down').length) {
            jQuery(this).find('svg.fa-chevron-down').removeClass('fa-chevron-down').addClass('fa-chevron-up');
        }else {
            jQuery(this).find('svg.fa-chevron-up').removeClass('fa-chevron-up').addClass('fa-chevron-down');
        }

    });

}
/***************************************************************************************
 *  sidebarFlyoutToggle() -
 *
 *  Open and close a sidebar
 ***************************************************************************************/
function sidebarFlyoutToggle() {
   jQuery('.uccs-sidebar-toggle').click(function (e) {
       //check to see if we're open or closed
       if(jQuery(this).hasClass('in')) {
           //already open, so close the sidebar
           jQuery(this).removeClass('in');
           jQuery('.uccs-sidebar-flyout').hide('500');
           // jQuery('.uccs-sidebar-toggle-left').animate({
           //     left: '0'
           // }, 500);
           // jQuery('.uccs-sidebar-toggle-right').animate({
           //     right: '0'
           // }, 500);
           jQuery('.uccs-sidebar-toggle svg').hide('250');
           jQuery('.uccs-sidebar-toggle svg').removeClass('fa-times');
           jQuery('.uccs-sidebar-toggle svg').addClass('fa-bars');
           jQuery('.uccs-sidebar-toggle svg').show('250');
       }else {
           // var left = '25vw';
           // if(window.innerWidth < 992) {
           //     left = '40vw';
           // }
           // if (window.innerWidth < 577) {
           //     left = 'calc(100vw - 5em)';
           // }

           //need to open the sidebar
           jQuery(this).addClass('in');
           jQuery('.uccs-sidebar-flyout').show('500');
           // jQuery('.uccs-sidebar-toggle-left').animate({
           //     left: left
           // }, 500);
           // jQuery('.uccs-sidebar-toggle-right').animate({
           //     right: left
           // }, 500);
           jQuery('.uccs-sidebar-toggle svg').hide('250');
           jQuery('.uccs-sidebar-toggle svg').removeClass('fa-bars');
           jQuery('.uccs-sidebar-toggle svg').addClass('fa-times');
           jQuery('.uccs-sidebar-toggle svg').show('250');
       }

   });

}
