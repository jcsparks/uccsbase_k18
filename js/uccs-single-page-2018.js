let locked = false;
let x0 = null;
let defaultMove = 200;  // default amount to move the slider on a single event
let InitLeft = 0;
let Animating = 0;

jQuery(document).ready(function () {

    const _SLIDER = document.querySelector('.uccs-sp-image-slider-slider');

    if(_SLIDER) {
        jQuery('.uccs-sp-image-slider-slider').each(function () {
            var thisSlider = jQuery(this).attr('id');
            UCCS_SP_Slider(thisSlider);
        });

        // handler to deal with clicking on the left and right control buttons we've provided
        jQuery('.sliderLeft-button').click(function (e) {
            e.preventDefault();
            var sliderScroll = jQuery(this).siblings('.uccs-sp-image-slider-slider').attr('id');
            scrollMe('right', defaultMove, sliderScroll);
        });
        jQuery('.sliderRight-button').click(function (e) {
            e.preventDefault();
            var sliderScroll = jQuery(this).siblings('.uccs-sp-image-slider-slider').attr('id');
            scrollMe('left', defaultMove, sliderScroll);
        });
    }

    //figure out the correct margin-top for the right column body in a SP Header Row
    spHeaderRightColHeight();

    var resizeID;
    jQuery(window).resize(function () {
        clearTimeout(resizeID);

        resizeID = setTimeout(spHeaderRightColHeight, 100);
    });

    // click handler for opening/closing an accordion row
    jQuery('.uccs-sp-accordion-toggle').click(function () {
       jQuery(this).closest('.uccs-sp-accordion-row-container').toggleClass('active');

       if(jQuery(this).find('svg.fa-plus').length) {
           jQuery(this).find('svg.fa-plus').removeClass('fa-plus').addClass('fa-minus');
       }else {
           jQuery(this).find('svg.fa-minus').removeClass('fa-minus').addClass('fa-plus');
       }
    });

    // click handler for the list items functionality
    jQuery('li.uccs-sp-list-item-row').click(function () {
        //reset the active flag on all the rows
        jQuery('li.uccs-sp-list-item-row').removeClass('active');

        // get the HTML from the content for that item
        var itemHTML = jQuery(this).find('.uccs-sp-list-content').html();

        // and put the selected content into the content container
        jQuery(this).parents('.uccs-sp-list-ul-container').siblings('.uccs-sp-list-details').html(itemHTML).addClass('block');

        // and set the clicked link to active
        jQuery(this).addClass('active');

        //see if we're on mobile and the content may be hidden
        var frameWidth = window.innerWidth;
        if(frameWidth < 577) {
            if(Animating) {
                return;
            }

            startAnimating();
            jQuery('html, body').animate({
                scrollTop: jQuery(this).parents('.uccs-sp-list-ul-container').siblings('.uccs-sp-list-details').offset().top
            }, 1000);
            doneAnimating();
        }

        // need this for Chrome to redraw the SVG filter properly after we've added the additional content
        jQuery('.uccs-sp-list-paragraph').toggleClass('force-redraw');

    });


    // look for any lightboxes
    UCCSlightbox();

});

function UCCS_SP_Slider(mySlider) {
    var sliderID = '#' + mySlider;
    // we need to number the slides
    var SlideCount = 0;
    jQuery(sliderID).find('.uccs-media-image-slider-container').each(function () {
        SlideCount++;
        if(SlideCount < 10) {
            jQuery(this).find('.uccs-media-image-slider-count').html('0' + SlideCount);
        } else {
            jQuery(this).find('.uccs-media-image-slider-count').html(SlideCount);
        }
    });

    var sliderEle = document.getElementById(mySlider);

    // handler to deal with dragging (touch and mouse) events
    sliderEle.addEventListener('mousedown', touchLock, false);
    sliderEle.addEventListener('touchstart', touchLock, false);

    sliderEle.addEventListener('mousemove', touchDrag, false);
    sliderEle.addEventListener('touchmove', touchDrag, false);

    sliderEle.addEventListener('mouseup', touchMove, false);
    sliderEle.addEventListener('touchend', touchMove, false);


    // Get IE or Edge browser version
    var MSBrowser = detectIE();

    if (MSBrowser) {
        jQuery(sliderID).css('background-image', 'none');
    }
}

/**
 * detect IE
 * returns version of IE or false, if browser is not Internet Explorer
 */
function detectIE() {
    const ua = window.navigator.userAgent;

    // Test values; Uncomment to check result …

    // IE 10
    // ua = 'Mozilla/5.0 (compatible; MSIE 10.0; Windows NT 6.2; Trident/6.0)';

    // IE 11
    // ua = 'Mozilla/5.0 (Windows NT 6.3; Trident/7.0; rv:11.0) like Gecko';

    // Edge 12 (Spartan)
    // ua = 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/39.0.2171.71 Safari/537.36 Edge/12.0';

    // Edge 13
    // ua = 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/46.0.2486.0 Safari/537.36 Edge/13.10586';

    let msie = ua.indexOf('MSIE ');
    if (msie > 0) {
        // IE 10 or older => return version number
        return parseInt(ua.substring(msie + 5, ua.indexOf('.', msie)), 10);
    }

    var trident = ua.indexOf('Trident/');
    if (trident > 0) {
        // IE 11 => return version number
        var rv = ua.indexOf('rv:');
        return parseInt(ua.substring(rv + 3, ua.indexOf('.', rv)), 10);
    }

    var edge = ua.indexOf('Edge/');
    if (edge > 0) {
        // Edge (IE 12+) => return version number
        return parseInt(ua.substring(edge + 5, ua.indexOf('.', edge)), 10);
    }

    // other browser
    return false;
}


function scrollMe(direction, slideOffset, slider) {
    // don't buffer event during the animation, just wait till we're done to start the next one
    let sliderID = "#" + slider;
    if(Animating) {
        return;
    }
    var myLeft = jQuery(sliderID).css('left').replace(/[^-\d\.]/g, '');
    var sliderContentWidth = jQuery(sliderID).parent('.uccs-sp-image-slider-content').width();
    var sliderWidth = jQuery(sliderID).width();

    var rightEdge = (sliderContentWidth - sliderWidth) - slideOffset;

    //if all slides are already visible there is no reason to slide
    if (rightEdge >= 0) {
        return;
    }

    if (direction == 'left') {
        myLeft = parseInt(myLeft) - slideOffset;
        if (myLeft < rightEdge) {
            shakeMe(100, 10, 4, slider);
            return;
        }
    } else {
        myLeft = parseInt(myLeft) + slideOffset;
        //alert(myLeft);
        if (myLeft >= slideOffset) {
            jQuery(sliderID).animate({
                left: slideOffset + 'px'
            }, 400, "linear", function () {
                shakeMe(100, 10, 4, slider);
            });
            return;
        }
    }
    // jQuery('.uccs-sp-image-slider-slider').css('left', myLeft + 'px');
    startAnimating();
    jQuery(sliderID).animate({
        left: myLeft + 'px'
    }, 500, "linear", function () {
        doneAnimating();
    });
}

// helper functions to start and stop the animation process...
function startAnimating() {
    Animating = 1;
}
function doneAnimating() {
    Animating = 0;
}


// add a little horizontal shake to the slider when we reach an edge to let the user know
// we can't move in that direction any further.
function shakeMe(interval, distance, times, slider) {
    let sliderID = "#" + slider;

    if(Animating) {
        return;
    }

    if (interval == 'undefined') {
        interval = 100;
    }

    if (distance == 'undefined') {
        distance = 10;
    }

    if (times == 'undefined') {
        times = 4;
    }

    var myLeft = jQuery(sliderID).css('left').replace(/[^-\d\.]/g, '');
    var distLeft = parseInt(myLeft) - distance;
    var distRight = parseInt(myLeft) + distance;

    startAnimating();
    for(var j = 0; j < times + 1; j++) {
        jQuery(sliderID).animate(
            {
                left: ((j % 2 == 0 ? distLeft : distRight))
            }, interval, function () {
                doneAnimating();
            });
    }
}

function unifyTouch(e) {
    return e.changedTouches ? e.changedTouches[0] : e;
}

function touchLock(e) {
    x0 = unifyTouch(e).clientX;
    locked = true;

    InitLeft = jQuery(this).css('left').replace(/[^-\d\.]/g, '');
}

function touchDrag(e) {
    e.preventDefault();

    if(locked) {
        let dx = unifyTouch(e).clientX - x0;

        //console.log("Dragging at : " + dx);

        if(dx) {
            var newLeft = parseInt(InitLeft) + dx;

            var sliderContentWidth = jQuery(this).parent('.uccs-sp-image-slider-content').width();
            var sliderWidth = jQuery(this).width();

            var rightEdge = (sliderContentWidth - sliderWidth) - defaultMove;

            // no room to scroll right
            if(newLeft > defaultMove) {
                return;
            }
            // no room to scroll left
            if(newLeft < rightEdge) {
                return;
            }
            jQuery(this).css('left', newLeft + 'px');
        }
    }
}

function touchMove(e) {
    if(locked) {
        locked = false;
   }
}

function spHeaderRightColHeight() {

    var defaultMarginTop = '400px';
    // This will return the width of the viewport
    var frameWidth = window.innerWidth;

    jQuery('.uccs-sp-header-banner').each(function () {

        // determine how tall the header title container is
        var headerTitleHeight = jQuery(this).find('.uccs-sp-header-title-container').height();
        var imageTop = jQuery(this).find('.uccs-sp-header-img').css('top');
        var imageHeight = jQuery(this).find('.uccs-sp-header-img').height();

        var marginTop = imageHeight - (headerTitleHeight - parseInt(imageTop));

        //alert('Height is : ' + headerTitleHeight + ' and Top is : ' + imageTop + ' and marginTop is : ' + marginTop);

        if(frameWidth > 992 && marginTop) {
            jQuery(this).siblings('.uccs-sp-header-content').find('.uccs-sp-header-body-right').css('margin-top', marginTop);
        }else if(frameWidth > 768) {
            jQuery(this).siblings('.uccs-sp-header-content').find('.uccs-sp-header-body-right').css('margin-top', defaultMarginTop);
        }else {
            jQuery(this).siblings('.uccs-sp-header-content').find('.uccs-sp-header-body-right').css('margin-top', '0');
        }
    });
}

function renderFeedCache(cacheURL, selector) {
    //alert("renderFeedCache has been invoked");
    if(cacheURL && selector) {
        //alert("trying to get : " + cacheURL);
        jQuery.ajax({
            url: cacheURL,
            timeout: 2000,
            dataType: 'jsonp',
            crossDomain: true,
            error: function(XMLHttpRequest, textStatus, errorThrown) {
                if(window.console) {
                    console.error("Error occurred in renderFeedCache() : " + errorThrown);
                }
            },
            success: function(data) {
                //alert("renderFeedCache() success : " + data.response)
                jQuery(selector).append(data.response);
            }
        });
    }
}

// function to setup a lightbox using fancybox
function UCCSlightbox() {
    jQuery().fancybox({
        selector : '#lightgallery a'
    });
}